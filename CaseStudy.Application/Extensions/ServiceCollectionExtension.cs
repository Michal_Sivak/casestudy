using CaseStudy.Application.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CaseStudy.Application.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IProductService, ProductService>();

            return services;
        }
    }
}