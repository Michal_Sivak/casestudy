using System;
using System.Threading.Tasks;
using CaseStudy.Interface.Dtos;

namespace CaseStudy.Application.Services
{
    public interface IProductService
    {
        Task<ProductListDto> GetAll();
        Task<PagedProductListDto> GetAll(int pageNumber, int pageSize);
        Task<ProductDto> Get(Guid id);
        Task<Guid> AddNew(CreateProductDto dto);
        Task Update(Guid id, UpdateProductDto dto);
        Task UpdateDescription(Guid id, UpdateDescriptionProductDto dto);
        Task Delete(Guid id);
    }
}