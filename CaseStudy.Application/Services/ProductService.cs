using System;
using System.Linq;
using System.Threading.Tasks;
using CaseStudy.Domain.Models;
using CaseStudy.Infrastructure.Abstraction.Repositories;
using CaseStudy.Interface.Dtos;

namespace CaseStudy.Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _repository;

        public ProductService(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task<ProductListDto> GetAll()
        {
            var products = await _repository.FindAll();

            var dtos = products.Select(model => new ProductDto()
            {
                Id = model.Id,
                Name = model.Name,
                ImgUri = model.ImgUri,
                Price = model.Price,
                Description = model.Description
            });

            return new ProductListDto()
            {
                Products = dtos
            };
        }

        public async Task<PagedProductListDto> GetAll(int pageNumber, int pageSize)
        {
            var products = await _repository.FindAll(pageNumber, pageSize);

            var dtos = products.Select(model => new ProductDto()
            {
                Id = model.Id,
                Name = model.Name,
                ImgUri = model.ImgUri,
                Price = model.Price,
                Description = model.Description
            });

            return new PagedProductListDto()
            {
                Products = dtos,
                PageNumber = pageNumber,
                PageSize = pageSize
            };
        }

        public async Task<ProductDto> Get(Guid id)
        {
            var model = await _repository.Find(id);

            return new ProductDto()
            {
                Id = model.Id,
                Name = model.Name,
                ImgUri = model.ImgUri,
                Price = model.Price,
                Description = model.Description
            };
        }

        public async Task<Guid> AddNew(CreateProductDto dto)
        {
            var model = new ProductModel()
            {
                Id = Guid.NewGuid(),
                Name = dto.Name,
                ImgUri = dto.ImgUri,
                Price = dto.Price ?? 0,
                Description = dto.Description
            };

            return await _repository.Create(model);
        }

        public async Task Update(Guid id, UpdateProductDto dto)
        {
            var model = await _repository.Find(id);

            model.Name = dto.Name;
            model.ImgUri = dto.ImgUri;
            model.Price = dto.Price ?? 0;
            model.Description = dto.Description;

            await _repository.Update(model);
        }
        
        public async Task UpdateDescription(Guid id, UpdateDescriptionProductDto dto)
        {
            var model = await _repository.Find(id);
            
            model.Description = dto.Description;

            await _repository.Update(model);
        }

        public async Task Delete(Guid id)
        {
            await _repository.Delete(id);
        }
    }
}