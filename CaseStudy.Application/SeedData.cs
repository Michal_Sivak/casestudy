using System;
using System.Collections.Generic;
using CaseStudy.Domain.Models;

namespace CaseStudy.Application
{
    public class SeedData
    {
        public static IEnumerable<ProductModel> Data { get; } = new[]
        {
            new ProductModel()
            {
                Id = new Guid("11111111-1111-1111-1111-111111111111"),
                Name = "Test Model 1",
                ImgUri = "Uri",
                Price = 100,
                Description = "This is a test model 1"
            },
            new ProductModel()
            {
                Id = new Guid("22222222-2222-2222-2222-222222222222"),
                Name = "Model 2",
                ImgUri = "Uri",
                Price = 500,
                Description = "This is a test model 2"
            },
            new ProductModel()
            {
                Id = new Guid("33333333-3333-3333-3333-333333333333"),
                Name = "Test 3",
                ImgUri = "Uri",
                Price = 2500
            }
        };
    }
}