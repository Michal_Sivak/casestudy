using System.Collections.Generic;

namespace CaseStudy.Interface.Dtos
{
    public class PagedProductListDto
    {
        public IEnumerable<ProductDto> Products { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}