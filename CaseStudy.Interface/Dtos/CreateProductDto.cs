using System.ComponentModel.DataAnnotations;

namespace CaseStudy.Interface.Dtos
{
    public class CreateProductDto
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string ImgUri { get; set; }
        
        [Required]
        public decimal? Price { get; set; }
        
        public string Description { get; set; }
    }
}