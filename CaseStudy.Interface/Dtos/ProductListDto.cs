using System.Collections.Generic;

namespace CaseStudy.Interface.Dtos
{
    public class ProductListDto
    {
        public IEnumerable<ProductDto> Products { get; set; }
    }
}