using System.ComponentModel.DataAnnotations;

namespace CaseStudy.Interface.Dtos
{
    public class UpdateDescriptionProductDto
    {
        [Required]
        public string Description { get; set; }
    }
}