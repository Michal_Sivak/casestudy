using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaseStudy.Application;
using CaseStudy.Application.Services;
using CaseStudy.Domain.Models;
using CaseStudy.Infrastructure.Abstraction.Repositories;
using CaseStudy.Infrastructure.Exceptions;
using CaseStudy.Interface.Dtos;
using FluentAssertions;
using Moq;
using Xunit;

namespace CaseStudy.UnitTests.Application
{
    public class ProductServiceTest
    {
        [Fact]
        public async Task GetAll_Returns_ProductListDto()
        {
            var expectedData = new ProductListDto()
            {
                Products = GetProducts().Select(x => new ProductDto()
                {
                    Id = x.Id,
                    Name = x.Name,
                    ImgUri = x.ImgUri,
                    Price = x.Price,
                    Description = x.Description
                })
            };
            
            var mock = new Mock<IProductRepository>();
            mock.Setup(repository => repository.FindAll()).ReturnsAsync(GetProducts());

            var service = new ProductService(mock.Object);

            var actualData = await service.GetAll();
            
            Assert.Equal(expectedData.Products.Count(), actualData.Products.Count());
            actualData.Should().BeEquivalentTo(expectedData);
        }
        
        [Fact]
        public async Task Get_Returns_Correct_ProductDto()
        {
            var model1 = GetProducts().ToList()[0];
            var model2 = GetProducts().ToList()[1];
            var model3 = GetProducts().ToList()[2];

            var expectedData = new ProductDto()
            {
                Id = model1.Id,
                Name = model1.Name,
                ImgUri = model1.ImgUri,
                Price = model1.Price,
                Description = model1.Description
            };
            
            var mock = new Mock<IProductRepository>();
            mock.Setup(repository => repository.Find(model1.Id)).ReturnsAsync(model1);
            mock.Setup(repository => repository.Find(model2.Id)).ReturnsAsync(model2);
            mock.Setup(repository => repository.Find(model3.Id)).ReturnsAsync(model3);

            var service = new ProductService(mock.Object);

            var actualData = await service.Get(model1.Id);
            
            actualData.Should().BeEquivalentTo(expectedData);
        }
        
        [Fact]
        public void Get_Throws_NotFoundException_When_Guid_Does_Not_Exist()
        {
            var model1 = GetProducts().ToList()[0];
            var model2 = GetProducts().ToList()[1];
            var model3 = GetProducts().ToList()[2];

            var mock = new Mock<IProductRepository>();
            mock.Setup(repository => repository.Find(model1.Id)).ReturnsAsync(model1);
            mock.Setup(repository => repository.Find(model2.Id)).ReturnsAsync(model2);
            mock.Setup(repository => repository.Find(model3.Id)).ReturnsAsync(model3);
            mock.Setup(repository => repository.Find(It.IsNotIn(model1.Id, model2.Id, model3.Id)))
                .ThrowsAsync(new NotFoundException());

            var service = new ProductService(mock.Object);
            
            Func<Task> func = async () => await service.Get(new Guid("11111111-1111-1111-1111-111111111112"));

            func.Should().ThrowExactly<NotFoundException>();
        }
        
        [Fact]
        public async Task AddNew_Creates_New_ProductDto()
        {
            var mock = new Mock<IProductRepository>();
            mock.Setup(repository => repository.Create(It.IsAny<ProductModel>()))
                .ReturnsAsync((ProductModel target) => target.Id);

            var service = new ProductService(mock.Object);

            var dto = new CreateProductDto()
            {
                Name = "Test 4",
                ImgUri = "Img",
                Price = 12501,
                Description = null
            };
            
            var actualData = await service.AddNew(dto);

            actualData.Should().NotBeEmpty();
        }

        [Fact]
        public async Task Update_Updates_ProductModel()
        {
            var model1 = GetProducts().ToList()[0];
            var model2 = GetProducts().ToList()[1];
            var model3 = GetProducts().ToList()[2];
            
            var mock = new Mock<IProductRepository>();
            mock.Setup(repository => repository.Find(model1.Id)).ReturnsAsync(model1);
            mock.Setup(repository => repository.Find(model2.Id)).ReturnsAsync(model2);
            mock.Setup(repository => repository.Find(model3.Id)).ReturnsAsync(model3);
            mock.Setup(repository => repository.Update(It.IsAny<ProductModel>()));

            var service = new ProductService(mock.Object);
            
            var dto = new UpdateProductDto()
            {
                Name = model1.Name,
                ImgUri = model1.ImgUri,
                Price = model1.Price,
                Description = "Test update"
            };
            
            await service.Update(model1.Id, dto);

            mock.Verify(x => x.Update(It.IsAny<ProductModel>()), 
                Times.Once());
        }
        
        [Fact]
        public async Task UpdateDescription_Updates_ProductModel()
        {
            var model1 = GetProducts().ToList()[0];
            var model2 = GetProducts().ToList()[1];
            var model3 = GetProducts().ToList()[2];
            
            var mock = new Mock<IProductRepository>();
            mock.Setup(repository => repository.Find(model1.Id)).ReturnsAsync(model1);
            mock.Setup(repository => repository.Find(model2.Id)).ReturnsAsync(model2);
            mock.Setup(repository => repository.Find(model3.Id)).ReturnsAsync(model3);
            mock.Setup(repository => repository.Update(It.IsIn(model1, model2, model3)));

            var service = new ProductService(mock.Object);
            
            var dto = new UpdateDescriptionProductDto()
            {
                Description = "Test update"
            };
            
            await service.UpdateDescription(model2.Id, dto);

            mock.Verify(x => x.Update(It.IsIn(model1, model2, model3)), 
                Times.Once());
        }
        
        [Fact]
        public async Task Delete_Deletes_ProductModel()
        {
            var model1 = GetProducts().ToList()[0];
            var model2 = GetProducts().ToList()[1];
            var model3 = GetProducts().ToList()[2];
            
            var mock = new Mock<IProductRepository>();
            mock.Setup(repository => repository.Find(model1.Id)).ReturnsAsync(model1);
            mock.Setup(repository => repository.Find(model2.Id)).ReturnsAsync(model2);
            mock.Setup(repository => repository.Find(model3.Id)).ReturnsAsync(model3);
            mock.Setup(repository => repository.Delete(It.IsIn(model1.Id, model2.Id, model3.Id)));

            var service = new ProductService(mock.Object);

            await service.Delete(model3.Id);

            mock.Verify(x => x.Delete(It.IsIn(model1.Id, model2.Id, model3.Id)), 
                Times.Once());
        }
        
        private static IEnumerable<ProductModel> GetProducts()
        {
            return SeedData.Data;
        }
        
        [Fact]
        public async Task GetAll_Returns_PagedProductListDto()
        {
            var products = GetProducts().Select(x => new ProductDto()
            {
                Id = x.Id,
                Name = x.Name,
                ImgUri = x.ImgUri,
                Price = x.Price,
                Description = x.Description
            }).ToList();
            
            var expectedDataFirstPage = new PagedProductListDto()
            {
                Products = products.Take(2),
                PageNumber = 1,
                PageSize = 2
            };
            
            var expectedDataSecondPage = new PagedProductListDto()
            {
                Products = products.Skip(2).Take(1),
                PageNumber = 2,
                PageSize = 1
            };
            
            var mock = new Mock<IProductRepository>();
            mock.Setup(repository => repository.FindAll(1, 2))
                .ReturnsAsync(GetProducts().Take(2));
            mock.Setup(repository => repository.FindAll(2, 1))
                .ReturnsAsync(GetProducts().Skip(2).Take(1));

            var service = new ProductService(mock.Object);

            var actualDataFirstPage = await service.GetAll(1, 2);
            var actualDataSecondPage = await service.GetAll(2, 1);
            
            Assert.Equal(expectedDataFirstPage.Products.Count(), actualDataFirstPage.Products.Count());
            Assert.Equal(expectedDataSecondPage.Products.Count(), actualDataSecondPage.Products.Count());
            actualDataFirstPage.Should().BeEquivalentTo(expectedDataFirstPage);
            actualDataSecondPage.Should().BeEquivalentTo(expectedDataSecondPage);
        }
    }
}