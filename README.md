# CaseStudy application

## Config

All configuration files can be found in `Inbox.Infrastructure.Configs` 
directory

You can configure them with User-Secrets for instance but there are numerous 
other possibilities

## Setup

### Database & Seed

There are 4 ways how to setup the database and seeding with `DatabaseConfig` 
which consists of 3 properties:

`ConnectionString`, default **null** -  classic connection string to the 
database

`UseSeedData`, default **true** - flag which specifies if we want to use 
seed data. They are located in `CaseStudy.Application`

`UseInMemoryDatabase`, default **false** - flag which specifies if we want 
to use in-memory database

#### 1. Use Database and Seed it

Note: If we want to use Database and forget to specify `ConnectionString`
the `ConfigurationValueMissingException` is thrown

Config:

```json
{
  "DatabaseConfig": {
    "ConnectionString": "<ConnectionString>",
    "UseSeedData": true,
    "UseInMemoryDatabase": false
  }
}
```

#### 2. Use Database and DON'T Seed it

Note: If we want to use Database and forget to specify `ConnectionString`
the `ConfigurationValueMissingException` is thrown

Config:

```json
{
  "DatabaseConfig": {
    "ConnectionString": "<ConnectionString>",
    "UseSeedData": false,
    "UseInMemoryDatabase": false
  }
}
```

#### 3. Use IN-MEMORY Database and Seed it

Note: If we set `ConnectionString` it is **ignored**

Config:

```json
{
  "DatabaseConfig": {
    "UseSeedData": true,
    "UseInMemoryDatabase": true
  }
}
```

#### 4. Use IN-MEMORY Database and DON'T Seed it

Note: If we set `ConnectionString` it is **ignored**

Config:

```json
{
  "DatabaseConfig": {
    "UseSeedData": false,
    "UseInMemoryDatabase": true
  }
}
```

## Tests

Tests can be run with `dotnet test` in `CaseStudy.UnitTests` directory or in any IDE with build-in functionality

## App

App can be run with `dotnet run` in `CaseStudy` directory or in any IDE with build-in functionality

## Swagger

API documentation is available here: `localhost:5001/swagger/index.html`. There is `v1` and `v2`

