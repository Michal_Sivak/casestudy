using System;
using CaseStudy.Infrastructure.Abstraction.Repositories;
using CaseStudy.Infrastructure.Configs;
using CaseStudy.Infrastructure.Exceptions;
using CaseStudy.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CaseStudy.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, 
            IConfiguration configuration)
        {
            var databaseConfig = configuration.GetSection("DatabaseConfig").Get<DatabaseConfig>();
            services.AddDbContext<CaseStudyContext>(builder =>
            {
                if (!databaseConfig.UseInMemoryDatabase)
                {
                    if (databaseConfig.ConnectionString == null)
                    {
                        throw new ConfigurationValueMissingException(
                            "ConnectionString to the database is not configured");
                    }
                    
                    builder.UseSqlServer(databaseConfig.ConnectionString);    
                }
                else
                {
                    builder.UseInMemoryDatabase("CaseStudy");   
                }
            });

            services.AddScoped<IProductRepository, ProductRepository>();
            
            return services;
        }
    }
}