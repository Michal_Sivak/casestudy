using CaseStudy.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace CaseStudy.Infrastructure
{
    public class CaseStudyContext : DbContext
    {
        public CaseStudyContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<ProductModel> Products { get; set; }
    }
}