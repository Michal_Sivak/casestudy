using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaseStudy.Domain.Models;
using CaseStudy.Infrastructure.Abstraction.Repositories;
using CaseStudy.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace CaseStudy.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly CaseStudyContext _context;

        public ProductRepository(CaseStudyContext caseStudyContext)
        {
            _context = caseStudyContext;
        }

        public async Task<IEnumerable<ProductModel>> FindAll()
        {
            return await _context.Products.ToListAsync();
        }

        public async Task<IEnumerable<ProductModel>> FindAll(int pageNumber, int pageSize)
        {
            return await _context.Products
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public async Task<ProductModel> Find(Guid id)
        {
            var model = await _context.Products.FindAsync(id);

            return model switch
            {
                null => throw new NotFoundException("Given ID not found"),
                _ => model
            };
        }

        public async Task Delete(Guid id)
        {
            var model = await Find(id);
            _context.Remove(model);
            await _context.SaveChangesAsync();
        }

        public async Task Update(ProductModel model)
        {
            _context.Update(model);
            await _context.SaveChangesAsync();
        }

        public async Task<Guid> Create(ProductModel model)
        {
            await _context.Products.AddAsync(model);
            await _context.SaveChangesAsync();

            return model.Id;
        }

        public async Task AddOrUpdate(ProductModel model)
        {
            try
            {
                var productModel = await Find(model.Id);

                productModel.Name = model.Name;
                productModel.ImgUri = model.ImgUri;
                productModel.Price = model.Price;
                productModel.Description = model.Description;

                await Update(productModel);
            }
            catch (NotFoundException)
            {
                await Create(model);
            }
        }
    }
}