namespace CaseStudy.Infrastructure.Configs
{
    public class DatabaseConfig
    {
        public string ConnectionString { get; set; }

        public bool UseSeedData { get; set; } = true;

        public bool UseInMemoryDatabase { get; set; } = false;
    }
}