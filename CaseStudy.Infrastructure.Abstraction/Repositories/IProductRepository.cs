using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CaseStudy.Domain.Models;

namespace CaseStudy.Infrastructure.Abstraction.Repositories
{
    public interface IProductRepository
    {
        Task<IEnumerable<ProductModel>> FindAll();
        Task<IEnumerable<ProductModel>> FindAll(int pageNumber, int pageSize);
        Task<ProductModel> Find(Guid id);
        Task Delete(Guid id);
        Task Update(ProductModel model);
        Task<Guid> Create(ProductModel model);
        Task AddOrUpdate(ProductModel model);
    }
}