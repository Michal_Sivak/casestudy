using System;
using System.Threading;
using System.Threading.Tasks;
using CaseStudy.Application;
using CaseStudy.Infrastructure.Abstraction.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CaseStudy.HostedServices
{
    public class DatabaseSeedHostedService : IHostedService
    {
        private readonly IServiceProvider _provider;
        private readonly bool _useSeedData;

        public DatabaseSeedHostedService(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _provider = serviceProvider;
            _useSeedData = configuration.GetValue<bool>("DatabaseConfig:UseSeedData");
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (_useSeedData)
            {
                using var scope = _provider.CreateScope();
                var context = scope.ServiceProvider.GetRequiredService<IProductRepository>();

                foreach (var productModel in SeedData.Data)
                {
                    await context.AddOrUpdate(productModel);
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}