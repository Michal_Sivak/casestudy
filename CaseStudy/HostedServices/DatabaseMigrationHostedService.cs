using System;
using System.Threading;
using System.Threading.Tasks;
using CaseStudy.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CaseStudy.HostedServices
{
    public class DatabaseMigrationHostedService : IHostedService
    {
        private readonly IServiceProvider _provider;
        private readonly bool _useInMemoryDatabase;

        public DatabaseMigrationHostedService(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _provider = serviceProvider;
            _useInMemoryDatabase = configuration.GetValue<bool>("DatabaseConfig:UseInMemoryDatabase");
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (!_useInMemoryDatabase)
            {
                using var scope = _provider.CreateScope();
                var context = scope.ServiceProvider.GetRequiredService<CaseStudyContext>();
            
                await context.Database.MigrateAsync(cancellationToken);   
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}