using System;
using System.Threading.Tasks;
using CaseStudy.Application.Services;
using CaseStudy.Infrastructure.Exceptions;
using CaseStudy.Interface.Dtos;
using CaseStudy.Interface.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CaseStudy.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/products")]
    [Produces("application/json")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;

        public ProductController(IProductService service)
        {
            _service = service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ProductListDto), StatusCodes.Status200OK)]
        [ApiVersion("1.0")]
        public async Task<ActionResult<ProductListDto>> Product()
        {
            return Ok(await _service.GetAll());
        }
        
        [HttpGet]
        [ProducesResponseType(typeof(PagedProductListDto), StatusCodes.Status200OK)]
        [ApiVersion("2.0")]
        public async Task<ActionResult<PagedProductListDto>> Product([FromQuery] PaginationFilter filter)
        {
            return Ok(await _service.GetAll(filter.PageNumber, filter.PageSize));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ProductDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ApiVersion("1.0")]
        [ApiVersion("2.0")]
        public async Task<ActionResult<ProductDto>> Product(Guid id)
        {
            try
            {
                var dto = await _service.Get(id);
                return Ok(dto);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [ProducesResponseType(typeof(CreateProductDto), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ApiVersion("1.0")]
        [ApiVersion("2.0")]
        public async Task<ActionResult> AddNew(CreateProductDto dto)
        {
            var guid = await _service.AddNew(dto);
            return Created($"/products/{guid}", dto);
        }

        [HttpPatch("{id}/description")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ApiVersion("1.0")]
        [ApiVersion("2.0")]
        public async Task<ActionResult> UpdateDescription(Guid id, UpdateDescriptionProductDto dto)
        {
            try
            {
                await _service.UpdateDescription(id, dto);
                return NoContent();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ApiVersion("1.0")]
        [ApiVersion("2.0")]
        public async Task<ActionResult> Update(Guid id, UpdateProductDto dto)
        {
            try
            {
                await _service.Update(id, dto);
                return NoContent();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ApiVersion("1.0")]
        [ApiVersion("2.0")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                await _service.Delete(id);
                return Ok();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }
    }
}