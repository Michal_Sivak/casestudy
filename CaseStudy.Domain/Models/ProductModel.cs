using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CaseStudy.Domain.Models
{
    public class ProductModel
    {
        public Guid Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string ImgUri { get; set; }
        
        [Required]
        [Column(TypeName = "decimal(18,4)")]
        public decimal Price { get; set; }
        
        public string Description { get; set; }
    }
}